<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist_Songs;

class Playlist_SongsController extends Controller
{
    public function displayPlaylistSongs(){
        return DB::table('Playlist_Songs')->get();
    }
    public function store(Request $request){

    $newPlaylists = new Playlist_Songs();
    $newPlaylists->song_id = $request->song_id;
    $newPlaylists->playlist_id = $request->playlist_id;
    $newPlaylists->save();
    return $newPlaylists;
}
}
