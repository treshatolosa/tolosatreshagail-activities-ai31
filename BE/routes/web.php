<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/songs', [SongsController::class, 'displaySongs']);
Route::post('/upload', [SongsController::class, 'store']);

Route::get('/playlist', [PlaylistsController::class, 'displayPlaylists']);
Route::post('/create', [PlaylistsController::class, 'store']);

Route::get('/playlistsongs', [Playlist_SongsController::class, 'displayPlaylistSongs']);
Route::post('/create', [Playlist_SongsController::class, 'store']);
