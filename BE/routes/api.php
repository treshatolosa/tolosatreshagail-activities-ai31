<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongsController;
use App\Http\Controllers\PlaylistsController;
use App\Http\Controllers\Playlist_SongsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/songs', [SongsController::class, 'displaySongs']);
Route::post('/upload', [SongsController::class, 'store']);

Route::get('/playlist', [PlaylistsController::class, 'displayPlaylists']);
Route::post('/create', [PlaylistsController::class, 'store']);

Route::get('/playlistsongs', [Playlist_SongsController::class, 'displayPlaylistSongs']);
Route::post('/create', [Playlist_SongsController::class, 'store']);